import React, { Component } from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import Login from './views/Login'
import Translation from './views/Translation'
import Profile from './views/Profile'


export class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <main className="App">
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/translation" component={Translation} />
            <Route path="/profile" component={Profile} />
            <Route exact path="/">
              <Redirect to="/login" />
            </Route>
          </Switch>
        </main>
      </BrowserRouter>

    )
  }
}

export default App
