import a from '../assets/images/a.png'
import b from '../assets/images/b.png'
import c from '../assets/images/c.png'
import d from '../assets/images/d.png'
import e from '../assets/images/e.png'
import f from '../assets/images/f.png'
import g from '../assets/images/g.png'
import h from '../assets/images/h.png'
import i from '../assets/images/i.png'
import j from '../assets/images/j.png'
import k from '../assets/images/k.png'
import l from '../assets/images/l.png'
import m from '../assets/images/m.png'
import n from '../assets/images/n.png'
import o from '../assets/images/o.png'
import p from '../assets/images/p.png'
import q from '../assets/images/q.png'
import r from '../assets/images/r.png'
import s from '../assets/images/s.png'
import t from '../assets/images/t.png'
import u from '../assets/images/u.png'
import v from '../assets/images/v.png'
import w from '../assets/images/w.png'
import x from '../assets/images/x.png'
import y from '../assets/images/y.png'
import z from '../assets/images/z.png'

const MOCK_IMAGES = [
    {
        name: 'a',
        url: a
    },
    {
        name: 'b',
        url: b
    },
    {
        name: 'c',
        url: c
    },
    {
        name: 'd',
        url: d
    },
    {
        name: 'e',
        url: e
    },
    {
        name: 'f',
        url: f
    },
    {
        name: 'g',
        url: g
    },
    {
        name: 'h',
        url: h
    },
    {
        name: 'i',
        url: i
    },
    {
        name: 'j',
        url: j
    },
    {
        name: 'k',
        url: k
    },
    {
        name: 'l',
        url: l
    },
    {
        name: 'm',
        url: m
    },
    {
        name: 'n',
        url: n
    },
    {
        name: 'o',
        url: o
    },
    {
        name: 'p',
        url: p
    },
    {
        name: 'q',
        url: q
    },
    {
        name: 'r',
        url: r
    },
    {
        name: 's',
        url: s
    },
    {
        name: 't',
        url: t
    },
    {
        name: 'u',
        url: u
    },
    {
        name: 'v',
        url: v
    },
    {
        name: 'w',
        url: w
    },
    {
        name: 'x',
        url: x
    },
    {
        name: 'y',
        url: y
    },
    {
        name: 'z',
        url: z
    }
];

export default MOCK_IMAGES
