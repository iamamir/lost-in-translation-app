import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import TranslationItem from '../components/TranslationItem'
import AddTranslation from '../components/AddTranslation'

export class Translation extends Component {
    state = {
        keyword: ''
    }
    profileButtonClicked = () => {
        this.props.history.push('/profile')
    }
    addTranslation = (keyword) => {
        this.setState({
            keyword: keyword
        })
    };

    render() {
        return (
            <div className="container">
                <div className="nav-wrapper">
                    <div className="left-side">
                        <div>
                            <h3> Lost in Translation</h3>
                        </div>
                    </div>

                    <div className="right-side">
                        <div className="nav-button-wrapper">
                            <button onClick={this.profileButtonClicked} className="Button ButtonSecondary">
                                {localStorage.getItem('__lit-us') || <Redirect to="/login" />}</button>
                        </div>
                    </div>
                </div>
                <div className="Form">
                    <div className="Container">
                        <AddTranslation addTranslation={this.addTranslation} />
                        <div className="Translation">
                            <TranslationItem keyword={this.state.keyword} />
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}


export default Translation
