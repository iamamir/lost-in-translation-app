import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import LoginForm from '../components/forms/LoginForm'
import styles from './Login.module.css'


export class Login extends Component {

    redirectToTranslation = (redirect) => {
        if (redirect)
            this.props.history.push('/translation')
    }

    render() {
        return (
            <div className="container">
                {localStorage.getItem('__lit-us') && <Redirect to="/translation" />}

                <div className="nav-wrapper">
                    <div className="left-side">
                                <h1 className="Title">Lost in Translation</h1>
                        
                    </div>
                </div>
                <div className="Form">
                    <div className="Container">
                        <header className={styles.LoginHeader}>
                            <h4 className="Subtitle">Login to start translating letters to signs</h4>
                        </header>
                        <LoginForm onSuccess={this.redirectToTranslation} />
                    </div>
                </div >

            </div>

        )

    }
}

export default Login

