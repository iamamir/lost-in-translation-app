import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Translations from '../components/Translations'


export class Profile extends Component {
    state = {
        keywords: JSON.parse(localStorage.getItem('__lit-keywords')) || []
    }

    translateButtonClicked = () => {
        this.props.history.push('/translation')

    }
    logoutButtonClicked = () => {
        if (localStorage.getItem('__lit-us'))
            localStorage.removeItem('__lit-us')

        if (localStorage.getItem('__lit-keywords'))
            localStorage.removeItem('__lit-keywords')
        this.props.history.push('/login')


    }
    render() {
        return (
            <div className="container">
                <div className="nav-wrapper">

                    <div className="left-side">
                        <div>
                            <h3> Hi {localStorage.getItem('__lit-us') || <Redirect to="/login" />} </h3>

                        </div>
                    </div>

                    <div className="right-side">

                        <div className="nav-button-wrapper">
                            <button onClick={this.translateButtonClicked} className="Button ButtonSecondary">Translate</button>
                        </div>
                        <div className="nav-button-wrapper">
                            <button onClick={this.logoutButtonClicked} className="Button ButtonSecondary">Logout</button>
                        </div>
                    </div>
                </div>
                <div className="Form ProfileDiv ">
                    <div className="Container ">
                        <Translations keywords={this.state.keywords} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile
