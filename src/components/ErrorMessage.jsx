import styles from './ErrorMessage.module.css'
import React from 'react'

const ErrorMessage = (props) => {

    let errorDisplay = null
    if (props.error) {
        errorDisplay = (
            <section className={styles.ErrorMessage}>
                <p>{props.error}</p>
            </section>
        )
    }
    return (
        <>
            {errorDisplay}
        </>
    )
}

export default ErrorMessage