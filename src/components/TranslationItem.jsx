import React from 'react'
import SignItem from './SignItem';
import { v4 as uuid } from 'uuid';

const TranslationItem = (props) => {
    let displayTranslation = null;
    if (props.keyword) {
        displayTranslation = (
            props.keyword.word.split('').map((letter) => (
                <SignItem key={uuid()} letter={letter} />
            ))
        )
    }

    return (
        <>
            
            {displayTranslation}

            <hr/>            
        </>
    )
}

export default TranslationItem
