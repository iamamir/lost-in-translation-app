import React, { Component } from 'react'
import MOCK_IMAGES from '../mocks/images.mock.js'


export class SignItem extends Component {
    render() {
        
        return (
            <>
                <img className="Image" src={MOCK_IMAGES.find(img => img.name === this.props.letter).url} alt={this.props.letter} />
            </>
        )
    }
}
export default SignItem
