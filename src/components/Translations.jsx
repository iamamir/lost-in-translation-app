import React, { Component } from 'react'
import TranslationItem from './TranslationItem'

export class Translations extends Component {
    render() {
        return this.props.keywords.map((keyword) => (
            <TranslationItem key={keyword.id} keyword={keyword} />
        ))
    }
}
export default Translations
