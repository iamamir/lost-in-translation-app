import React, { Component } from 'react'
import ErrorMessage from '../ErrorMessage'

export class LoginForm extends Component {
    state = {
        username: '',
        loginError: '',
        redirect: false
    }

    onUserNameChanged = (e) => {
        this.setState({
            username: e.target.value.trim()
        })
    }

    onLoginClicked = () => {
        if (this.state.username.length < 3) {
            this.setState({
                loginError: 'Username is too short!Minimum 2 characters please!'
            })
        }
        else {
            this.setState({
                redirect: true
            }, function () { this.props.onSuccess(this.state.redirect) })
            localStorage.setItem('__lit-us', this.state.username)
        }
    }


    render() {
        return (
            <>
                <section className="LoginForm">
                    <section className="InputGroup">
                        <label className="InputLabel" htmlFor="username">Username</label>
                        <input onChange={this.onUserNameChanged} className="Input" type="text" placeholder="Enter your username..." />
                    </section>
                    <button onClick={this.onLoginClicked} className="Button ButtonPrimary">Login</button>
                </section>
                <br />
                <ErrorMessage error={this.state.loginError} />
            </>
        )
    }
}

export default LoginForm