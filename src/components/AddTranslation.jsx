import React, { Component } from 'react'
import { v4 as uuid } from 'uuid';

export class AddTranslation extends Component {
    state = {
        word: '',
        keywords: JSON.parse(localStorage.getItem('__lit-keywords')) || []


    }

    onTranslateChange = (e) => {
        this.setState({
            word: e.target.value.trim().replace(/[^A-Za-z\s]/g, "").replace(/\s+/g, "").toLowerCase(),

        })
    }

    addTranslation = (e) => {
        e.preventDefault();

        if (this.state.word.length > 1) {
            const newKeyword = {
                word: this.state.word,
                id: uuid()
            }

            if (this.state.keywords.length < 10) {
                this.setState({
                    keywords: [...this.state.keywords, newKeyword]
                }, function () {
                    this.setTranslation()
                })
            }
            else {
                this.setState({
                    keywords: [...this.state.keywords.splice(1, this.state.keywords.length), newKeyword]
                }, function () {
                    this.setTranslation()

                })
            }

        }
        this.setState({ word: '' })
    };

    setTranslation = () => {
        localStorage.setItem('__lit-keywords', JSON.stringify(this.state.keywords))
        this.props.addTranslation(this.state.keywords[this.state.keywords.length - 1]);
    }

    render() {
        return (
            <>
                <section className="InputGroup TranslationForm">
                    <input onChange={this.onTranslateChange} className="Input" type="text" placeholder="Write something to translate..." />
                    <button onClick={this.addTranslation} className="Button ButtonPrimary">Translate</button>

                </section>

            </>
        )
    }
}

export default AddTranslation
